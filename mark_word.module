<?php

/**
 * The main file for quiz words question type.
 */

/**
 * Implementation of hook_help().
 */
function mark_word_help($path, $args) {
  if ($path == 'admin/help#mark_word') {
    return t('This module provides a quiz words question type for Quiz.');
  }
}

function mark_word_init() {

  }

/**
 * Implementation of hook_quiz_question_info().
 */
function mark_word_quiz_question_info() {
  return array(
    'mark_word' => array(
      'name' => t('Mark the words'),
      'description' => t('Quiz questions that find noun or verb in the question.'),
      'question provider' => 'MarkWordQuestion',
      'response provider' => 'MarkWordResponse',
      'module' => 'quiz_question', // All wrapper functions are in that module.
    ),
  );
}

/**
 * Implementation of the quiz_question hook_config.
 */
function mark_word_config() {
  return FALSE; // No config options available for this question type
}


/**
 * Implementation of hook_autoload_info().
 */
function mark_word_autoload_info() {
  return array(
    'MarkWordQuestion' => array('file' => 'mark_word.classes.inc'),
    'MarkWordResponse' => array('file' => 'mark_word.classes.inc'),
  );
}

/**
 * Implementation of hook_form_alter()
 */
function mark_word_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'mark_word_node_form') {
    $form['#after_build'][] = 'mark_word_node_form_after_build';
  }
}

function mark_word_node_form_after_build($form) {
  // To prevent wysiwyg editor messing with question syntax
  $form['body'][LANGUAGE_NONE]['0']['format']['#access'] = FALSE;
  $form['body'][LANGUAGE_NONE]['0']['#wysiwyg'] = FALSE;
  return $form;
}

/**
 * Implementation of hook_theme().
 */
function mark_word_theme() {
  $path = drupal_get_path('module', 'mark_word') . '/theme';
  return array(
    'mark_word_response_form' => array(
      'render element' => 'form',
      'path' => $path,
      'file' => 'mark_word.theme.inc',
    ),
    'mark_word_user_answer' => array(
      'arguments' => array('answer' => NULL, 'correct' => NULL),
      'path' => $path,
      'file' => 'mark_word.theme.inc',
    ),
    'mark_word_answering_form' => array(
      'render element' => 'form',
      'path' => $path,
      'file' => 'mark_word.theme.inc',
    ),
  );
}

function _mark_word_shuffle_choices($choices) {
  $new_array = array();
  $new_array[''] = '';
  while (count($choices)) {
    $element = array_rand($choices);
    $new_array[$element] = $choices[$element];
    unset($choices[$element]);
  }
  return $new_array;
}

function _mark_word_get_correct_answer($question, $admin_answer) {
  $output = '';
  $chunks = explode(' ', $question);
  $answer = explode(',', $admin_answer);
  $correct_answer = array();
  foreach ($chunks as $key => $chunk) {
    if (in_array($chunk, $answer, TRUE)) {
      $correct_answer[] =  '<span class="answer correct correct-answer">' . $chunk . '</span>';
      continue;
    }
    $correct_answer[] =  $chunk;
  }
  $output = implode(' ', $correct_answer);
  return str_replace("\n", "<br/>", $output);
}

function _mark_word_get_user_answer($question, $user_answer, $admin_answer) {
  $output = '';
  $question = explode(' ', $question);
  $user_answer = explode(',', $user_answer);
  $admin_answer = explode(',', $admin_answer);
  $correct_answer = array();
  foreach ($question as $key => $chunk) {
    if (in_array($chunk, $user_answer, TRUE)) {
      $array_intersect = array_intersect($user_answer, $admin_answer);
      $class = (in_array($chunk, $array_intersect)) ? 'correct' : 'incorrect';
      $class .= ' answer user-answer';
      $correct_answer[$key] = '<span class="' . $class . '">' . $chunk . '</span>';
      continue;
    }
    $correct_answer[$key] =  $chunk;
  }
  $output = implode(' ', $correct_answer);
  return str_replace("\n", "<br/>", $output);
}

